//Nick Burns
//SDL Practice 1 -- Hello World
//This also satisfies the "get image on a screen" requirement of Lab 8

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include <string>
using namespace std;

//set screen attributes
const int WIDTH = 1000;
const int HEIGHT = 1000;
const int RESOLUTION = 32;


//images
SDL_Surface* pic = NULL;
SDL_Surface* screen = NULL;


SDL_Surface *load_image(string filename) {

  //temp storage for the image
  SDL_Surface* tempImage = NULL;

  //optimized image to be used
  SDL_Surface* optimizedImage = NULL;

  //load the image
  tempImage = SDL_LoadBMP(filename.c_str());

  //if the image loaded properly
  if (tempImage != NULL) {
    //create an optimized image
    optimizedImage = SDL_DisplayFormat(tempImage);

    //free the old image
    SDL_FreeSurface(tempImage);
  }

  //return the optimized image
  return optimizedImage;

}



void apply_surface (int x, int y, SDL_Surface* source, SDL_Surface* destination) {

  //make a temp rectangle to hold the offsets
  SDL_Rect offset;

  //Give the offsets to the rectangle
  offset.x = x;
  offset.y = y;

  //blit the surface
  SDL_BlitSurface(source, NULL, destination, &offset);

}





int main(int argc, char* args[]) {

  //initialize SDL, making sure it works
  if (SDL_Init(SDL_INIT_EVERYTHING) == -1) {
    return 1;
  }

  //set up the screen
  screen = SDL_SetVideoMode(WIDTH, HEIGHT, RESOLUTION, SDL_SWSURFACE);

  //make sure there were no problems setting up the screen
  if (screen == NULL) {
    return 1;
  }

  //set the window caption
  SDL_WM_SetCaption("Frog!!!", NULL);


  //load image
  pic = load_image("frog3.bmp");

  //put frog on screen
  apply_surface(300, 300, pic, screen);

  //update the screen
  if (SDL_Flip(screen) == -1) {
    return 1;
  }

  //pause so we can see the frog (2 seconds)
  SDL_Delay(3000);

  //free the frog
  SDL_FreeSurface(pic);

  //quit SDL
  SDL_Quit();

  return 0;

}
