//Nick Burns
//SDL Practice 1 -- Hello World
//This also satisfies the "get image on a screen" requirement of Lab 8

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include <string>
using namespace std;

//set screen attributes
const int WIDTH = 750;
const int HEIGHT = 750;
const int RESOLUTION = 32;

//set frog attributes
const int PIC_WIDTH = 500;
const int PIC_HEIGHT = 330;

//event structure
SDL_Event event;


//images
SDL_Surface* pic = NULL;
SDL_Surface* screen = NULL;



//the frog class that contains the picture
class Frog {
  
public:
  Frog();    //constructor to initialize things
  void handle_input();
  void move();
  void display();

private:
  int x, y;
  int dx, dy;

};

//frog functions
Frog::Frog() {

  //initialize position and velocity
  x = 0;
  y = 0;
  dx = 0;
  dy = 0;

}

void Frog::handle_input() {


  /*
  //if a key was pressed
  if (event.type == SDL_KEYDOWN) {
    //set velocity in appropriate direction
    switch(event.key.keysym.sym) {
    case SDLK_UP:
      dy = -3;
      break;
    case SDLK_DOWN:
      dy = 3;
      break;
    case SDLK_LEFT:
      dx = -3;
      break;
    case SDLK_RIGHT:
      dx = 3;
      break;
    }
  } else if (event.type == SDL_KEYUP) {      //if a key was released
    //set velocity to 0
    switch(event.key.keysym.sym) {
    case SDLK_UP:
      dy = 0;
      break;
    case SDLK_DOWN:
      dy = 0;
      break;
    case SDLK_LEFT:
      dx = 0;
      break;
    case SDLK_RIGHT:
      dx = 0;
      break;
    }
  }
*/



  //check to see which keys are pressed and handle movement accordingly

  
  //get keystates
    Uint8* keystates = SDL_GetKeyState(NULL);

    //determine which arrow keys are currently pressed and adjust velocity
    //up key
    if (keystates[SDLK_UP]) {
      dy = -3;
    } else if (!keystates[SDLK_DOWN]) {
      dy = 0;
    }

    //down key
    if (keystates[SDLK_DOWN]) {
      dy = 3;
    } else if (!keystates[SDLK_UP]) {
      dy = 0;
    }

    //left key
    if (keystates[SDLK_LEFT]) {
      dx = -3;
    } else if (!keystates[SDLK_RIGHT]) {
      dx = 0;
    }

    //right key
    if (keystates[SDLK_RIGHT]) {
      dx = 3;
    } else if (!keystates[SDLK_LEFT]) {
      dx = 0;
    }
  


}



void Frog::move() {

  //move horizontally
  x += dx;

  //make sure it doesn't move off the screen
  if (x < 0) {
    x = 0;
  } else if (x > (WIDTH - PIC_WIDTH)) {
    x = WIDTH - PIC_WIDTH;
  }

  //move vertically
  y += dy;

  //make sure it doesn't move off the screen
  if (y < 0) {
    y = 0;
  } else if (y > (HEIGHT - PIC_HEIGHT)) {
    y = HEIGHT - PIC_HEIGHT;
  }

}


void apply_surface (int x, int y, SDL_Surface* source, SDL_Surface* destination) {

  //make a temp rectangle to hold the offsets
  SDL_Rect offset;

  //Give the offsets to the rectangle
  offset.x = x;
  offset.y = y;

  //blit the surface
  SDL_BlitSurface(source, NULL, destination, &offset);

}



void Frog::display() {

  //draw the frog
  apply_surface(x, y, pic, screen);

}




SDL_Surface *load_image(string filename) {

  //temp storage for the image
  SDL_Surface* tempImage = NULL;

  //optimized image to be used
  SDL_Surface* optimizedImage = NULL;

  //load the image
  tempImage = SDL_LoadBMP(filename.c_str());

  //if the image loaded properly
  if (tempImage != NULL) {
    //create an optimized image
    optimizedImage = SDL_DisplayFormat(tempImage);

    //free the old image
    SDL_FreeSurface(tempImage);
  }

  //return the optimized image
  return optimizedImage;

}








//function to initialize everything--screen, SDL, etc.
bool init() {

  //initialize SDL stuff
  if (SDL_Init(SDL_INIT_EVERYTHING) == -1) {
    return false;
  }

  //set up screen
  screen = SDL_SetVideoMode(WIDTH, HEIGHT, RESOLUTION, SDL_SWSURFACE);

  //check for errors in setting up screen
  if (screen == NULL) {
    return 1;
  }
  
  //set window title
  SDL_WM_SetCaption("Frog!!!", NULL);

  //if everything initialized properly
  return true;

}



//function to load the necessary files
bool load_files() {

  //load image
  pic = load_image("frog3.bmp");

  //make sure image loaded properly
  if (pic == NULL) {
    return false;
  }

  //if it loaded fine
  return true;

}


//function to clean up everything when we're done
void cleanUp() {

  //free the frog
  SDL_FreeSurface (pic);

  //quit SDL
  SDL_Quit();

}





int main(int argc, char* args[]) {

  //make the program wait for the user to X out the window
  bool quit = false;
  
  //initialize everything
  if (init() == false){ 
    return 1;
  }

  //load all files
  if (load_files() == false) {
    return 1;
  }

  //create a frog object
  Frog froggy;

  //put frog on screen
  //apply_surface(300, 300, pic, screen);




  //main program loop
  while (quit == false) {

    //while there's an event waiting
    while(SDL_PollEvent(&event)) {

      //handle input for frog
      froggy.handle_input();

      /*
      //if a key was pressed
      if (event.type == SDL_KEYDOWN) {

	//do the proper thing based on which key
	switch (event.key.keysym.sym) {
	case SDLK_UP:
	  //do things
	  break;
	case SDLK_DOWN:
	  //get down
	  break;
	case SDLK_LEFT:
	  //shuffle left
	  break;
	case SDLK_RIGHT:
	  //slide right
	  break;
	}

      }
      */


      //if user exes out window
      if (event.type == SDL_QUIT) {
	//quit the program
	quit = true;
      }

    } //end event polling

    //move the frog
    froggy.move();

    //clear the screen
    SDL_FillRect( screen, &screen->clip_rect, SDL_MapRGB( screen->format, 0xFF, 0xFF, 0xFF ) );


    //draw the frog
    froggy.display();

    //update the screen
    if (SDL_Flip(screen) == -1) {
      return 1;
    }

  }



  //free everything and quit
  cleanUp();

  return 0;

}
